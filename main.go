package main

import (
	"bitbucket.org/ValdasPilkauskas/websockets/stringReplacement"
	"bitbucket.org/ValdasPilkauskas/websockets/stringReplacement/controller/v1"
	"flag"
	"fmt"
	"github.com/gorilla/websocket"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

func main() {
	startTime := time.Now()

	portPtr := flag.Int("port", 3000, "Port number")
	flag.Parse()

	baseUrl := fmt.Sprintf(":%d", *portPtr)
	staticHtml, err := getStaticHtml(*portPtr)
	if err != nil {
		panic(err)
	}

	upgrader := websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
	}

	whiteSpaceReplacementService := stringReplacement.NewService(" ", "$")
	whiteSpaceCtrl := v1.NewWhiteSpaceReplacementController(whiteSpaceReplacementService, upgrader)

	http.HandleFunc("/stringReplacement/v1/whiteSpace", whiteSpaceCtrl.ReplaceWhiteSpacesHandler)

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, staticHtml)
	})

	log.Printf("Listening on - [%s]\n", baseUrl)
	log.Printf("Application started in - [%fms]\n", float64(time.Since(startTime).Nanoseconds())/1e6)
	log.Fatalf("ERROR - %s", http.ListenAndServe(baseUrl, nil))
}

func getStaticHtml(servingPort int) (string, error) {
	indexHtml, err := os.Open("html/index.html")
	if err != nil {
		return "", err
	}

	index, err := ioutil.ReadAll(indexHtml)
	if err != nil {
		return "", err
	}

	externalPort := os.Getenv("SERVING_PORT")
	if externalPort == "" {
		externalPort = strconv.Itoa(servingPort)
	}

	return strings.Replace(string(index), "${PORT}", externalPort, 1), nil
}
