FROM alpine:3.6

EXPOSE 3000

COPY ./html /app/html
COPY ./websockets /app/websockets

WORKDIR /app

ENTRYPOINT [ "/app/websockets" ]

