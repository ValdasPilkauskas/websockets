# WebSocket server example

[![Go Report Card](https://goreportcard.com/badge/bitbucket.org/ValdasPilkauskas/websockets)](https://goreportcard.com/report/bitbucket.org/ValdasPilkauskas/websockets)

* Dependencies
    * go get
        * go get github.com/gorilla/websocket
        * go get github.com/google/uuid
    * you can also download dependencies using trash by executing **```trash```**
        * to setup trash please take a look into official repo - https://github.com/rancher/trash

* Building
    * Execute **```go build```** in project root directory

* Running locally
    * To run application on default port (3000) execute **```./websockets```**
    * To run application on different port execute **```./websocket -port=8080```**
    
* Running in docker
    1. Build application with this command - **```GOOS=linux GOARCH=amd64 go build```**
    2. Execute this command in project root directory - **```docker-compose up --build```**

* Running tests
    * To execute all tests run **```go test ./..```**
* Accessing application
    * Websocket Api is hosted at ws://{baseUrl}:{selectedPort}/stringReplacement/v1/whiteSpace
    * Application also comes with simple (really simple) ui which can be reached at http://{baseUrl}:{selectedPort}/ for example - http://localhost:3000/