package stringReplacement

import "strings"

type StringReplacerService struct {
	target      string
	replacement string
}

func NewService(target string, replacement string) *StringReplacerService {
	return &StringReplacerService{
		target:      target,
		replacement: replacement,
	}
}

func (replacer *StringReplacerService) ReplaceString(input string) string {
	return strings.Replace(input, replacer.target, replacer.replacement, -1)
}
