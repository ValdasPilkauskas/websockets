package stringReplacement

import "testing"

func TestNewService(t *testing.T) {
	service := NewService(" ", "$")

	if service.target != " " {
		t.Errorf("Target string is not correct. Was expecting - [%s], but [%s] was found instead", " ", service.target)
	}

	if service.replacement != "$" {
		t.Errorf("Replacement string is not correct. Was expecting - [%s], but [%s] was found instead", "$", service.replacement)
	}
}

var replaceString_SuccessfullScenarios = []struct {
	input          string
	expectedResult string
}{
	{"Train", "Train"},
	{"Train....   ....  ...", "Train....$$$....$$..."},
	{"!@& # % * & & ^!@$&*", "!@&$#$%$*$&$&$^!@$&*"},
	{"Train      Mc'TrainFace      .      ", "Train$$$$$$Mc'TrainFace$$$$$$.$$$$$$"},
}

func TestStringReplacerService_ReplaceString(t *testing.T) {
	service := NewService(" ", "$")

	for _, value := range replaceString_SuccessfullScenarios {
		result := service.ReplaceString(value.input)

		if result != value.expectedResult {
			t.Errorf("Service failed to replace string. Was expceting [%s], but [%s] was returned instead\n", value.expectedResult, result)
		}
	}
}
