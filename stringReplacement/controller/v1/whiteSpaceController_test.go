package v1

import (
	"bitbucket.org/ValdasPilkauskas/websockets/stringReplacement"
	"github.com/gorilla/websocket"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func setUpController() WhiteSpaceReplacementController {
	service := stringReplacement.NewService(" ", "$")
	upgrd := websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
	}

	return *NewWhiteSpaceReplacementController(service, upgrd)
}

func TestWhiteSpaceReplacementController_ReplaceWhiteSpacesHandler_SuccessfulRequest(t *testing.T) {
	log.SetOutput(ioutil.Discard)
	controller := setUpController()

	s := httptest.NewServer(http.HandlerFunc(controller.ReplaceWhiteSpacesHandler))
	defer s.Close()

	u := "ws" + strings.TrimPrefix(s.URL, "http")
	ws, _, err := websocket.DefaultDialer.Dial(u, nil)
	if err != nil {
		t.Fatalf("%v", err)
	}
	defer ws.Close()

	for i := 0; i < 100; i++ {
		if err := ws.WriteMessage(websocket.TextMessage, []byte("Space le space")); err != nil {
			t.Fatalf("Failed to send message. Error - [%v]", err)
		}
		_, p, err := ws.ReadMessage()
		if err != nil {
			t.Fatalf("Failed to read message. Error - [%v]", err)
		}

		expectedMessage := "Space$le$space"
		if string(p) != expectedMessage {
			t.Fatalf("Wrong message specified. Was expecting - [%s], but [%s] was returned instead", expectedMessage, p)
		}
	}
}

func BenchmarkWhiteSpaceReplacementController_ReplaceWhiteSpacesHandler(b *testing.B) {
	url := "ws://localhost:3000/stringReplacement/v1/whiteSpace"

	ws, _, err := websocket.DefaultDialer.Dial(url, nil)
	if err != nil {
		b.Fatalf("%v", err)
	}
	defer ws.Close()

	for i := 0; i < 100; i++ {
		if err := ws.WriteMessage(websocket.TextMessage, []byte("Space le space")); err != nil {
			b.Fatalf("Failed to send message. Error - [%v]", err)
		}
		_, p, err := ws.ReadMessage()
		if err != nil {
			b.Fatalf("Failed to read message. Error - [%v]", err)
		}

		expectedMessage := "Space$le$space"
		if string(p) != expectedMessage {
			b.Fatalf("Wrong message specified. Was expecting - [%s], but [%s] was returned instead", expectedMessage, p)
		}
	}
}
