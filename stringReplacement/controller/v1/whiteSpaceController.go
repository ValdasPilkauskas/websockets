package v1

import (
	"bitbucket.org/ValdasPilkauskas/websockets/stringReplacement"
	"github.com/google/uuid"
	"github.com/gorilla/websocket"
	"log"
	"net/http"
)

type WhiteSpaceReplacementController struct {
	stringReplacementService stringReplacement.StringReplacerService
	wsUpgrader               websocket.Upgrader
}

func NewWhiteSpaceReplacementController(srs *stringReplacement.StringReplacerService,
	wsUpgrader websocket.Upgrader) *WhiteSpaceReplacementController {
	return &WhiteSpaceReplacementController{
		stringReplacementService: *srs,
		wsUpgrader:               wsUpgrader,
	}
}

func (wsrpc *WhiteSpaceReplacementController) ReplaceWhiteSpacesHandler(w http.ResponseWriter, r *http.Request) {
	connectionId := uuid.New().String()
	log.Printf("Received new connection - [%s]\n", connectionId)

	conn, err := wsrpc.wsUpgrader.Upgrade(w, r, nil)
	defer conn.Close()

	if err != nil {
		log.Printf("Failed to upgrade connection - [%s]. Error - %v\n", connectionId, err)
		return
	}
	log.Printf("Connection was upgraded successfully - [%s]", connectionId)

	for {
		msgType, msg, err := conn.ReadMessage()
		if err != nil {
			if !websocket.IsCloseError(err, websocket.CloseNormalClosure, websocket.CloseGoingAway) {
				log.Printf("Failed to read message - [%s]. Error - %s\n", connectionId, err)
				return
			}
			log.Printf("Closing connection for - [%s]", connectionId)
			return
		}

		replacedString := wsrpc.stringReplacementService.ReplaceString(string(msg))

		err = conn.WriteMessage(msgType, []byte(replacedString))
		if err != nil {
			log.Printf("Failed to send message - connectionId[%s]. Error - %v\n", connectionId, err)
			return
		}
	}
}
